
const model = require('../models') // inisiasi variabel model dari folder model




module.exports = {
    list: async (req, res) => {
        try {
            const datas = await model.car.findAll()
            return res.render('list', {
                card_cars: datas
            })
            

        } catch (eror) {
            return res.status(500).json({
                "message" : error,
                "data" : null
            })
        }
    },
    create: async(req, res) => {
            const data = await model.car.create(req.body)
            return res.redirect('/car/list')
    },
    update: async(req, res) => {
        
        try {
            const data = await model.car.update({
                name: req.body.name,
                price: req.body.price,
                size: req.body.size,
                photo: req.body.photo
            }, {
                where: {
                    id: req.params.id
                }
            })
                
            return res.redirect('/car/list')
        } catch (eror) {
            return res.status(500).json({
                "message" : error,
                "data" : null
            })
        }
    },

    destroy: async(req, res) => {
        try {
            const data = await model.car.destroy({
                where: {
                    id: req.params.id
                }
            })
            return res.redirect('/car/list')
        } catch (eror) {
            return res.status(500).json({
                "message" : error,
                "data" : null
            })
        }
    }
    
}