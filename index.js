require('dotenv').config() // untuk membaca file.env
const express = require('express') // inisiasi variabel yang berisi express
const app  = express() // inisiasi function express ke variabel app
const cors = require('cors')
const bodyParser = require('body-parser')
const exphbs = require('express-handlebars')
const path = require('path')
const router = require('./router/router.js') //mengambil router.js
const port = process.env.PORT || 3500 // deklarasi port dari env
const model = require('./models') // inisiasi variabel model dari folder model


app.set('views', 'views');
app.set('view engine', 'hbs');
app.use(express.static("views"));
app.use("/static", express.static("views")); //menggunakan /static untuk semua file di folder views


app.use(express.urlencoded({ extended: true}))
app.use(bodyParser.json()) // menggunakan fungsi dari body-parser untuk menangkap json request
app.use(cors())
app.use('/', router)

app.get('/car/create', (req, res) => {
    res.render('create')
})

app.get('/car/update', (req, res) => {
    model.car.findOne({
        where: {
          id: req.query.id,
        },
      }).then((car) => {
        // res.status(200).json(car);
        console.log(car);
        const { id, name, price, size, photo } = car;
        const data = {
            id, name, price, size, photo
        }
        res.render('update', data)
      });

    
})
app.listen(port, () => {
    console.log(`Server running on port ${port}`);
})