const express = require('express') //inisiasi variabel yang berisi express
const router = express.Router() // inisiasi variable yang berisi fungsi router express
const { list, create, update, destroy} = require('../controller/carController') //inisiasi object di car controller

router.get('/list',list) // route untuk endpoint bagian list (menampilkan)
router.post('/create', create) // route untuk endpoint bagian create (membuat)
router.post('/update/:id', update)
router.get('/delete/:id', destroy) 

module.exports = router // export ini berfungsi agar module lain bisa membaca file ini