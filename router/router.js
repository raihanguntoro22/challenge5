const express = require('express') //inisiasi variabel yang berisi express
const router = express.Router() // inisiasi variable yang berisi fungsi router express
const carRouter = require('./car') //inisiasi router car

router.use('/car', carRouter) //implementasi route car dengan /car




module.exports = router // export ini berfungsi agar module lain bisa membaca file ini